# brav-ml
Évaluation machine learning - Recherche et mise en application

Exploration d'un jeu de données du NYPD qui recense les différents usages de la force par ses membres.
Les informations disponibles sont:
- TRI (identifiant d'incident)
- Member Gender
- Member Injured
- ForceType
- InjuryLevel
- Race
- Assignment
- Rank Grouped

L'objectif est de faire des prédictions sur une certaine information en renseignant les autres sur un modèle de test.

### Pré-requis
- Python 3.11 (utilisation d'énumérations)

### Installation
- Installer les dépendances avec pip:
```shell
pip install -r requirements.txt
``` 

### Utilisation

##### Lancer le script principal:
```shell
py main.py
```
##### L'invite de commande va demander un certain nombre d'informations:

- Choisir une information à prédire:
```
Select what to predict:
0: Member Gender
1: Member Injured
2: ForceType
3: InjuryLevel
4: Race
5: Assignment
6: Rank Grouped

> 3
> Info to predict: InjuryLevel
```

- Remplir les caractéristiques du modèle à tester:
```
Is injured :
0: NOT_INJURED
1: IS_INJURED
> 1

Select the gender:
0: FEMALE
1: MALE
> 1

Select the force type :
[...]
```

- Définir la taille de la base de test pour l'entraînement du modèle:
```
Taille de la base de test (0.2 -> 0.4):
```

##### Puis le programme affiche les précisions de trois modèles entraînés
```
Précision du modèle de la régression logique après entraînement :  98.89164914962737
Précision du modèle de l'arbre de classification après entraînement :  98.86616981973374
Précision du modèle de random forest après entraînement :  98.87253965220715
```

##### Et enfin, les prédictions qu'il propose suivant les différents algorythmes d'entraînement:
```
Prédiction obtenue par le modèle LogisticRegression : Physical Injury
Prédiction obtenue par le modèle DecisionTreeClassifier : Physical Injury
Prédiction obtenue par le modèle RandomForestClassifier : Physical Injury
```

On peut essayer différentes combinaisons, créer différents modèles (agents de police) pour essayer de prédire une des informations.