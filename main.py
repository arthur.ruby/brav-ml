import random
import nypd_force as ny
import utils


if __name__ == '__main__':
    # Récupération des données:
    df = ny.create_df_from_file('datasets/NYPD_Use_of_Force__Members_of_Service.csv')

    # Ajout de la colonne année
    #df['Incident Year'] = (df['TRI Incident Number'] <= 1).astype(int)
    #df['Incident Year'] = df['TRI Incident Number'].apply(lambda x : str(x)[:4])

    # Suppression de la colonne d'identifiant:
    cols_to_remove = ['TRI Incident Number']
    df_dropped_tri = ny.remove_column(df, cols_to_remove)

    # Sélection de la colonne à prédire:
    y_col = utils.select_col_to_predict()

    # Création du modèle à prédire:
    model_to_predict = utils.create_model_to_predict(y_col)

    # Transfo des valeurs textuelles binaires en booléens:
    cols_to_encode = ['Member Gender', 'Member Injured']
    try:
        # Suppression de la colonne à prédire, si elle en fait partie.
        cols_to_encode.remove(y_col)
    except ValueError:
        pass
    df_encoded_cols = ny.encode_cols(dataframe=df_dropped_tri, cols_to_encode=cols_to_encode)

    # Transfo des valeurs différentes en one hot:
    cols_to_tansform = ['ForceType', 'InjuryLevel', 'Race', 'Assignment', 'Rank Grouped']
    try:
        # Suppression de la colonne à prédire, si elle en fait partie.
        cols_to_tansform.remove(y_col)
    except ValueError:
        pass
    df_col_transformed = df_encoded_cols
    for col in cols_to_tansform:
        df_col_transformed = ny.transfo_one_hot(dataframe=df_col_transformed, column=col)

    # Création d'une base d'apprentissage et de test
    random_state = random.randint(0, 100)
    print(f"random_state = {random_state}")
    y = df_col_transformed[y_col]
    x = df_col_transformed.drop(y_col, axis=1)
    x_train, x_test, y_train, y_test = ny.creation_apprentissage_test(x, y, random_state)

    # Entraînement du modèle:
    model_reg_log = ny.regression_logistique(x_train, x_test, y_train, y_test, random_state)
    model_tree_class = ny.arbre_classification(x_train, x_test, y_train, y_test, random_state)
    model_rand_forest = ny.random_forest(x_train, x_test, y_train, y_test, random_state)

    # Prédictions:
    models = [model_reg_log, model_tree_class, model_rand_forest]
    for model in models:
        ny.prediction(model, model_to_predict)
