import pandas as pd
from sklearn import linear_model, tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder


def create_df_from_file(filename):
    return pd.read_csv(filename)


def transfo_one_hot(dataframe, column):
    data_one_hot = pd.get_dummies(dataframe[column], prefix=column)
    dataframe_concat = pd.concat((dataframe, data_one_hot), axis=1)
    return dataframe_concat.drop(column, axis=1)


def encode_cols(dataframe, cols_to_encode):
    for col in cols_to_encode:
        # Définition d'un encodeur pour une colonne
        encoder = LabelEncoder().fit(dataframe[col].unique())
        # Encodage de la colonne
        dataframe[col] = encoder.transform(dataframe[col])
    return dataframe


def remove_column(dataframe, columns):
    for column in columns:
        dataframe.drop(column, axis=1, inplace=True)
    return dataframe


def creation_apprentissage_test(x, y, random_state):
    test_size = float(input("Taille de la base de test (0.2 -> 0.4): "))
    return train_test_split(x, y, test_size=test_size, random_state=random_state)


def regression_logistique(x_train, x_test, y_train, y_test, random_state):
    # Application de la régression logistique pour créer un modèle
    modele_reg = linear_model.LogisticRegression(max_iter=500, random_state=random_state)
    modele_reg.fit(x_train.values, y_train)

    # Calcul de la précision du modèle
    precision_reg = modele_reg.score(x_test.values, y_test)
    print("Précision du modèle de la régression logique après entraînement : ", precision_reg * 100)
    return modele_reg


def arbre_classification(x_train, x_test, y_train, y_test, random_state):
    # Application d'un arbre de décision de classification
    modele_class_tree = tree.DecisionTreeClassifier(max_depth=200000, random_state=random_state)
    modele_class_tree.fit(x_train.values, y_train)

    # Calculer la précision du modèle
    precision_class_tree = modele_class_tree.score(x_test.values, y_test)
    print("Précision du modèle de l'arbre de classification après entraînement : ", precision_class_tree * 100)
    return modele_class_tree


def random_forest(x_train, x_test, y_train, y_test, random_state):
    # Application de la random forest pour créer un modèle
    modele_rf = RandomForestClassifier(n_estimators=200, max_depth=100, random_state=random_state)
    modele_rf.fit(x_train.values, y_train)

    # Calculer la précision du modèle
    precision_rf = modele_rf.score(x_test.values, y_test)
    print("Précision du modèle de random forest après entraînement : ", precision_rf * 100)
    return modele_rf


def prediction(model, model_to_predict):
    resultat = model.predict(model_to_predict)
    print(f"Prédiction obtenue par le modèle {model.__class__.__name__} : {resultat[0]}")
