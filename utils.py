from enums import Column, Gender, ForceType, Injured, InjuryLevel, Race, Assignment, Rank


def set_gender(gender=0):
    """
    Sets the gender of the model to predict:

    0: Female

    1: Male
    :param gender: The gender value according to the doc.
    :type gender: int
    :return: A list containing the gender value.
    :rtype: list[int]
    """
    female = [0]
    male = [1]
    if gender == 0:
        return female
    return male


def set_injured(injured=0):
    """
    Sets the injury of the model to predict:

    0: No injury

    1: Injured
    :param injured: The injury value according to the doc.
    :type injured: int
    :return: A list containing the injury value.
    :rtype: list[int]
    """
    injury = [0]
    no_injury = [1]
    if injured == 0:
        return injury
    return no_injury


def set_forcetype(forcetype=4):
    """
    Sets the forcetype of the model to predict:

    0: Electrical Weapon

    1: Firearm

    2: Impact Weapon

    3: OC Spray

    4: Physical Force

    5: Police Canine

    6: Restraining Mesh Blanket
    :param forcetype: The forcetype value according to the doc.
    :type forcetype: int
    :return: A list containing the forcetype value.
    :rtype: list[int]
    """
    electric = [1, 0, 0, 0, 0, 0, 0]
    firearm = [0, 1, 0, 0, 0, 0, 0]
    impact = [0, 0, 1, 0, 0, 0, 0]
    spray = [0, 0, 0, 1, 0, 0, 0]
    physical = [0, 0, 0, 0, 1, 0, 0]
    canine = [0, 0, 0, 0, 0, 1, 0]
    blanket = [0, 0, 0, 0, 0, 0, 1]

    match forcetype:
        case 0:
            return electric
        case 1:
            return firearm
        case 2:
            return impact
        case 3:
            return spray
        case 4:
            return physical
        case 5:
            return canine
        case 6:
            return blanket
        case _:
            return physical


def set_injury_level(injury_level=4):
    """
    Sets the injury_level of the model to predict:

    0: Electrical Weapon

    1: Firearm

    2: Impact Weapon

    3: OC Spray

    4: Physical Force

    5: Police Canine

    6: Restraining Mesh Blanket
    :param injury_level: The injury_level value according to the doc.
    :type injury_level: int
    :return: A list containing the injury_level value.
    :rtype: list[int]
    """
    no_injury = [1, 0, 0, 0]
    physical_injury = [0, 1, 0, 0]
    serious_injury = [0, 0, 1, 0]
    substantial_injury = [0, 0, 0, 1]

    match injury_level:
        case 0:
            return no_injury
        case 1:
            return physical_injury
        case 2:
            return serious_injury
        case 3:
            return substantial_injury
        case _:
            return physical_injury


def set_race(race=0):
    """
    Sets the race of the model to predict:

    0: Amer Indian

    1: Asian

    2: Black

    3: Hispanic

    4: Other

    5: White
    :param race: The race value according to the doc.
    :type race: int
    :return: A list containing the race value.
    :rtype: list[int]
    """
    amer_indian = [1, 0, 0, 0, 0, 0]
    asian = [0, 1, 0, 0, 0, 0]
    black = [0, 0, 1, 0, 0, 0]
    hispanic = [0, 0, 0, 1, 0, 0]
    other = [0, 0, 0, 0, 1, 0]
    white = [0, 0, 0, 0, 0, 1]

    match race:
        case 0:
            return amer_indian
        case 1:
            return asian
        case 2:
            return black
        case 3:
            return hispanic
        case 4:
            return other
        case 5:
            return white
        case _:
            return amer_indian


def set_assignment(assignment=7):
    """
    Sets the assignment of the model to predict:

    0: Anti-crime

    1: Fixed post

    2: Investigation

    3: NCO

    4: Off-duty

    5: Other

    6: Scooter

    7: Sector/Response Auto
    :param assignment: The assignment value according to the doc.
    :type assignment: int
    :return: A list containing the assignment value.
    :rtype: list[int]
    """
    anti_crime = [1, 0, 0, 0, 0, 0, 0, 0]
    fixed_post = [0, 1, 0, 0, 0, 0, 0, 0]
    investigation = [0, 0, 1, 0, 0, 0, 0, 0]
    nco = [0, 0, 0, 1, 0, 0, 0, 0]
    off_duty = [0, 0, 0, 0, 1, 0, 0, 0]
    other = [0, 0, 0, 0, 0, 1, 0, 0]
    scooter = [0, 0, 0, 0, 0, 0, 1, 0]
    sector_response = [0, 0, 0, 0, 0, 0, 0, 1]

    match assignment:
        case 0:
            return anti_crime
        case 1:
            return fixed_post
        case 2:
            return investigation
        case 3:
            return nco
        case 4:
            return off_duty
        case 5:
            return other
        case 6:
            return scooter
        case 7:
            return sector_response
        case _:
            return sector_response


def set_rank(rank=7):
    """
    Sets the rank of the model to predict:

    0: Captain

    1: Chief

    2: Deputy Inspector

    3: Detective

    4: Inspector

    5: Lieutenant

    6: Other

    7: Police Officer

    8: RECODE

    9: Sergeant
    :param rank: The assignment value according to the doc.
    :type rank: int
    :return: A list containing the rank value.
    :rtype: list[int]
    """
    captain = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    chief = [0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
    deputy_inspector = [0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
    detective = [0, 0, 0, 1, 0, 0, 0, 0, 0, 0]
    inspector = [0, 0, 0, 0, 1, 0, 0, 0, 0, 0]
    lieutenant = [0, 0, 0, 0, 0, 1, 0, 0, 0, 0]
    other = [0, 0, 0, 0, 0, 0, 1, 0, 0, 0]
    officer = [0, 0, 0, 0, 0, 0, 0, 1, 0, 0]
    recode = [0, 0, 0, 0, 0, 0, 0, 0, 1, 0]
    sergeant = [0, 0, 0, 0, 0, 0, 0, 0, 0, 1]

    match rank:
        case 0:
            return captain
        case 1:
            return chief
        case 2:
            return deputy_inspector
        case 3:
            return detective
        case 4:
            return inspector
        case 5:
            return lieutenant
        case 6:
            return other
        case 7:
            return officer
        case 8:
            return recode
        case 9:
            return sergeant
        case _:
            return recode


def select_col_to_predict():
    col_number = -1
    col_value = ''
    while col_number not in range(len(Column)):
        prompt = "Select what to predict:"
        for col in list(Column):
            prompt += f"\n{list(Column).index(col)}: {col.value}"
        col_number = int(input(prompt + "\n"))
        col_value = list(Column)[col_number].value
    print(f"Info to predict: {col_value}")
    return col_value


def select_gender():
    gender_number = -1
    while gender_number not in range(len(Gender)):
        prompt = "Select the gender:"
        for gender in list(Gender):
            prompt += f"\n{gender.value}: {gender.name}"
        gender_number = int(input(prompt + "\n"))
    return gender_number


def select_injured():
    injured_number = -1
    while injured_number not in range(len(Injured)):
        prompt = "Is injured :"
        for injured in list(Injured):
            prompt += f"\n{injured.value}: {injured.name}"
        injured_number = int(input(prompt + "\n"))
    return injured_number


def select_forcetype():
    forcetype_number = -1
    while forcetype_number not in range(len(ForceType)):
        prompt = "Select the force type :"
        for forcetype in list(ForceType):
            prompt += f"\n{forcetype.value}: {forcetype.name}"
        forcetype_number = int(input(prompt + "\n"))
    return forcetype_number


def select_injury_level():
    injury_level_number = -1
    while injury_level_number not in range(len(InjuryLevel)):
        prompt = "Select the injury level :"
        for injury_level in list(InjuryLevel):
            prompt += f"\n{injury_level.value}: {injury_level.name}"
        injury_level_number = int(input(prompt + "\n"))
    return injury_level_number


def select_race():
    race_number = -1
    while race_number not in range(len(Race)):
        prompt = "Select the race :"
        for race in list(Race):
            prompt += f"\n{race.value}: {race.name}"
        race_number = int(input(prompt + "\n"))
    return race_number


def select_assignment():
    assignment_number = -1
    while assignment_number not in range(len(Assignment)):
        prompt = "Select the assignment :"
        for assignment in list(Assignment):
            prompt += f"\n{assignment.value}: {assignment.name}"
        assignment_number = int(input(prompt + "\n"))
    return assignment_number


def select_rank():
    rank_number = -1
    while rank_number not in range(len(Rank)):
        prompt = "Select the rank :"
        for rank in list(Rank):
            prompt += f"\n{rank.value}: {rank.name}"
        rank_number = int(input(prompt + "\n"))
    return rank_number


def create_model_to_predict(y_col: str):
    injured = [] if y_col == Column.INJURED.value else set_injured(select_injured())
    gender = [] if y_col == Column.GENDER.value else set_gender(select_gender())
    forcetype = [] if y_col == Column.FORCETYPE.value else set_forcetype(select_forcetype())
    injury_level = [] if y_col == Column.INJURY.value else set_injury_level(select_injury_level())
    race = [] if y_col == Column.RACE.value else set_race(select_race())
    assignment = [] if y_col == Column.ASSIGNMENT.value else set_assignment(select_assignment())
    rank = [] if y_col == Column.RANK.value else set_rank(select_rank())
    return [injured + gender + forcetype + injury_level + race + assignment + rank]
