from enum import Enum


class Gender(Enum):
    FEMALE = 0
    MALE = 1


class Injured(Enum):
    NOT_INJURED = 0
    IS_INJURED = 1


class ForceType(Enum):
    ELECTRICAL = 0
    FIREARM = 1
    IMPACT = 2
    SPRAY = 3
    PHYSICAL = 4
    CANINE = 5
    RESTRAINING_BLANKET = 6


class InjuryLevel(Enum):
    NO_INJURY = 0
    PHYSICAL = 1
    SERIOUS = 2
    SUBSTANTIAL = 3


class Race(Enum):
    AMERINDIAN = 0
    ASIAN = 1
    BLACK = 2
    HISPANIC = 3
    OTHER = 4
    WHITE = 5


class Assignment(Enum):
    ANTI_CRIME = 0
    FIXED_POST = 1
    INVESTIGATION = 2
    NCO = 3
    OFF_DUTY = 4
    OTHER = 5
    SCOOTER = 6
    RESPONSE_AUTO = 7


class Rank(Enum):
    CAPTAIN = 0
    CHIEF = 1
    DEPUTY_INSPECTOR = 2
    DETECTIVE = 3
    INSPECTOR = 4
    LIEUTENANT = 5
    OTHER = 6
    OFFICER = 7
    RECODE = 8
    SERGEANT = 9


class Column(Enum):
    GENDER = 'Member Gender'
    INJURED = 'Member Injured'
    FORCETYPE = 'ForceType'
    INJURY = 'InjuryLevel'
    RACE = 'Race'
    ASSIGNMENT = 'Assignment'
    RANK = 'Rank Grouped'
